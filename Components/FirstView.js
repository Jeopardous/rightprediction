/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';
import { useNetInfo } from "@react-native-community/netinfo";


const FirstView = (props) => {
    const { name, retry, loader } = props
    const netinfo = useNetInfo();

    return (
        <>
            <SafeAreaView>
                <Text>I am {name} Boss</Text>
                <Text>{netinfo.isConnected ? "Online" : "Offline"}</Text>
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({

});

export default FirstView;
