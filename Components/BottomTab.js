import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text, Button,
    StatusBar,
} from 'react-native';
import NetInfo, { useNetInfo } from "@react-native-community/netinfo";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome5'
import HomeScreen from '../Screens/HomeScreen';
import GameScreen from '../Screens/GameScreen';
import ErrorLoader from './ErrorLoader';
NetInfo.configure();


const BottomTabs = createBottomTabNavigator();

const BottomTab = ({ route }) => {

    console.log("Route Name:::", route)
    const netinfo = useNetInfo();
    const [isLoading, setLoading] = useState(false)


    if (isLoading) {
        return (<ErrorLoader retry={!netinfo.isConnected} loader={true} />)
    }
    return (
        <>
            <StatusBar barStyle="light-content" />

            <BottomTabs.Navigator initialRouteName={route.name}

                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        let iconName;

                        if (route.name === 'Home') {
                            iconName = focused
                                ? 'home'
                                : 'home';
                        } else if (route.name === 'Game') {
                            iconName = focused ? 'horse-head' : 'horse-head';
                        }

                        // You can return any component that you like here!
                        return <AwesomeIcon name={iconName} size={size} color={color} />;
                    },
                })}
                tabBarOptions={{
                    activeTintColor: 'tomato',
                    inactiveTintColor: 'gray',
                }}>
                <BottomTabs.Screen name="Home" component={HomeScreen} />
                <BottomTabs.Screen name="Game" component={GameScreen} />

            </BottomTabs.Navigator>


        </>
    );
};

const styles = StyleSheet.create({

});

export default BottomTab;