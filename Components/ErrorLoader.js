/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar, ActivityIndicator
} from 'react-native';


const ErrorLoader = (props) => {
    const { loader, retry, error, errorMessage } = props

    if (retry) {
        return (
            <>
                <SafeAreaView>
                    <Text>Retry</Text>
                </SafeAreaView>
            </>
        )
    }
    if (loader) {
        return (
            <ActivityIndicator size="large" />
        )
    }


    return (
        <>
            <SafeAreaView>
                <Text>Nothing Happened</Text>
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({

});

export default ErrorLoader;
