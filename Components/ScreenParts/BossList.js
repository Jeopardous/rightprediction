import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView, Image,
    View, TouchableOpacity,
    Text, Button, FlatList,
    StatusBar, Dimensions
} from 'react-native';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome5'
import Fonts from '../../Constants/FontSize'
import BossColors from '../../Constants/BossColors'


const { width, height } = Dimensions.get("window")

const BossList = (props) => {
    const {
        navigation,
        bodyStyle,
        listType = {

        },
        promotionIconName,
        promotionView,
        onPress,
        data,
        onIconPress,
        gridStyle,
        item, index

    } = props

    if (listType.type == 'simple') {
        return (
            <View style={[{
                width: "100%", minHeight: 40, height: 60, maxHeight: 120, borderRadius: 5, elevation: 2, alignItems: "center",
                backgroundColor: "#dddddd", marginVertical: 5, paddingHorizontal: 5, flexDirection: "row"
            }, listType.style.height < 40 ? null : listType.style.height > 120 ? { height: 120 } :
                listType.extraFields.length > 0 ? {
                    height: listType.style.height ? listType.style.height + listType.extraFields.slice(0, 4).length * 10 :
                        60 + listType.extraFields.slice(0, 4).length * 10
                } : listType.style]}>
                <TouchableOpacity
                    onPress={onPress}
                    style={{ flexDirection: "row", width: "90%", alignItems: "center", height: "100%" }} >
                    <Image source={require('../../Assets/medical.png')}
                        style={[{
                            width: listType.extraFields.slice(0, 4).length > 0 ? listType.style.height ?
                                (listType.style.height + listType.extraFields.slice(0, 4).length * 10) * 80 / 100
                                : (60 + listType.extraFields.slice(0, 4).length * 10) * 80 / 100 :
                                listType.style.height ? listType.style.height < 40 ? 60 * 80 / 100 :
                                    listType.style.height > 120 ? 120 * 80 / 100 :
                                        listType.style.height * 80 / 100 : 60 * 80 / 100, borderWidth: 1,
                            maxHeight: "80%", borderRadius: 0, borderColor: "#000000"
                        }, listType.imageStyle]}
                        resizeMode={"cover"}
                    />
                    <View style={[{
                        height: "100%", width: "100%", /* alignContent: "center", */ justifyContent: null,
                        paddingHorizontal: 10, paddingVertical: 5
                    }, listType.textContainerStyle]}>
                        <Text numberOfLines={1} style={[{ fontSize: Fonts.LARGE_FONT, fontWeight: "bold" }, listType.titleStyle]}>
                            {item.name}
                        </Text>
                        {listType.extraFields && listType.extraFields.length > 0 &&
                            listType.extraFields.slice(0, 4).map((item, index) => {
                                return (
                                    <Text key={index} numberOfLines={2} style={[{ fontSize: Fonts.SMALL_FONT }, listType.extraFieldsStyle]}>
                                        {item.title}
                                    </Text>
                                )
                            })
                        }
                        <Text numberOfLines={2} style={[{ fontSize: Fonts.SMALL_FONT }, listType.descriptionStyle]}>
                            {item.surname}
                        </Text>
                    </View>
                </TouchableOpacity>
                {listType.iconName && listType.iconName != "" && <TouchableOpacity
                    onPress={() => alert("index" + index)}
                    style={{
                        width: "10%", justifyContent: "center",
                        alignContent: "center", alignItems: "center", height: "100%"
                    }}>
                    <AwesomeIcon name={listType.iconName} size={listType.iconSize} style={[{}, listType.iconStyle]} />
                </TouchableOpacity>}
            </View>
        )
    }
    if (listType.type == 'grid') {
        return (
            <View style={[{
                width: listType.numColumns > 1 ?
                    width / listType.numColumns - 14 : width - 17,
                height: listType.numColumns == 2 ? width / 3 :
                    listType.numColumns == 3 ? width / (2.5) : listType.numColumns == 4 ?
                        width / (4) : listType.numColumns == 5 ? width / 4.5 : width / (2 + (0.5 * listType.numColumns)), marginHorizontal: 5,
                marginVertical: 5, backgroundColor: "#dddddd", elevation: 2, borderRadius: 5,
            }, gridStyle]}>
                <Image style={{ width: "100%", height: "100%", borderRadius: 5, resizeMode: "cover" }}
                    source={require('../../Assets/background.jpeg')} />
                <View style={{
                    position: "absolute", bottom: 0, right: 0, left: 0, flexDirection: "row",
                    height: "20%", backgroundColor: "rgba(0,0,0,0)", alignItems: "center",
                    paddingHorizontal: 5, paddingVertical: 5
                }}>
                    <AwesomeIcon name={"play"} size={10} style={{ color: BossColors.White }} />
                    <Text numberOfLines={1} ellipsizeMode={"middle"}
                        style={[{
                            marginHorizontal: 10, color: BossColors.WhiteText,
                            fontSize: Fonts.SMALL_FONT
                        }]}>Play this video</Text>

                </View>

                {promotionView && <View style={{
                    position: "absolute", top: 0, right: 0, left: 0, flexDirection: "row",
                    height: "20%", backgroundColor: "rgba(0,0,0,0)", alignItems: "center",
                    paddingHorizontal: 5, paddingVertical: 5
                }}>
                    {promotionIconName != "" && <AwesomeIcon name={promotionIconName} size={10} style={{ color: BossColors.RedText }} />}
                    <Text numberOfLines={1} ellipsizeMode={"middle"}
                        style={[{
                            marginHorizontal: 10, color: BossColors.WhiteText,
                            fontSize: Fonts.SMALL_FONT
                        }]}>Live</Text>

                </View>}

            </View>
        )
    }

};

const styles = StyleSheet.create({

});

export default BossList;
