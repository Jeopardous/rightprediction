import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View, TouchableOpacity,
    Text, Button,
    StatusBar, Dimensions
} from 'react-native';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome5'
import Fonts from '../../Constants/FontSize'
import BossColors from '../../Constants/BossColors'


const { width, height } = Dimensions.get("window")

const BossHeader = (props) => {
    const {
        navigation,
        headerLeftIconName,
        headerLeftIconStyle,
        headerRightIcons = [],
        headerRightIconsStyle,
        headerTitle,
        headerTitleStyle,
        headerStyle,
        onPress
    } = props
    return (
        <View style={[{ width: "100%", height: "9%", flexDirection: "row", backgroundColor: BossColors.AppBackground },
            headerStyle]}>
            {headerLeftIconName && headerLeftIconName != "" && <TouchableOpacity onPress={onPress}
                style={[{ width: "15%", height: "100%", justifyContent: "center", alignItems: "center" }]}>
                <AwesomeIcon name={headerLeftIconName} size={24} style={[{ color: BossColors.IconColor }, headerLeftIconStyle]} />

            </TouchableOpacity>}
            {headerTitle != "" && <View style={{
                width: headerLeftIconName && headerLeftIconName != "" ?
                    headerRightIcons.length == 0 ? "85%" : "40%" :
                    headerRightIcons.length == 0 ? "100%" : "55%",
                height: "100%", justifyContent: "center"
            }}>
                <Text style={[{ fontSize: Fonts.LARGE_FONT, fontWeight: "bold", paddingLeft: headerLeftIconName && headerLeftIconName != "" ? 0 : 10 },
                    headerTitleStyle]}>Home</Text>
            </View>}
            {headerRightIcons && headerRightIcons.length > 0 && <View style={{
                width: "45%", height: "100%", justifyContent: "flex-end", alignContent: "center",
                alignItems: "center", flexDirection: "row", paddingHorizontal: 5,
            }}>
                {/* <ScrollView style={{ borderWidth: 1, height: "100%", alignContent: "center" }}
                    contentContainerStyle={{ position: "relative", left: 0, right: 0, height: 50, alignItems: "center" }}
                    horizontal showsHorizontalScrollIndicator={false}> */}
                {headerRightIcons.map((item, index) => {
                    return (
                        <AwesomeIcon key={index} name={item.name} onPress={item.onPress}
                            size={20}
                            style={[{ marginHorizontal: 10, color: BossColors.IconColor }, headerRightIconsStyle]} />
                    )
                })}
                {/* </ScrollView> */}
            </View>}
        </View>
    )

};

const styles = StyleSheet.create({

});

export default BossHeader;
