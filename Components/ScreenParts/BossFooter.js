import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View, TouchableOpacity,
    Text, Button,
    StatusBar, Dimensions
} from 'react-native';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome5'
import Fonts from '../../Constants/FontSize'
import BossColors from '../../Constants/BossColors'


const { width, height } = Dimensions.get("window")

const BossFooter = (props) => {
    const {
        navigation,
        footerStyle,
        footerButtons = [],
    } = props
    return (
        <View style={[{
            width: "100%", height: "9%", position: "absolute", bottom: 0, justifyContent: "space-around", alignItems: "center",
            right: 0, left: 0, flexDirection: "row", backgroundColor: BossColors.AppBackground, alignContent: "center",
        }, footerStyle]}>
            {footerButtons && footerButtons.length > 0 && footerButtons.map((item, index) => {
                return (
                    <TouchableOpacity key={index} style={{
                        width: "40%", height: "70%", borderRadius: 5, backgroundColor: BossColors.AppGreen,
                        justifyContent: "space-around", alignItems: "center", flexDirection: item.iconDirection == "left" ? "row" : "row-reverse"
                    }} onPress={item.onPress}>
                        <AwesomeIcon name={item.iconName} size={item.iconSize} style={{}} />
                        <Text style={{ fontSize: item.titleSize, fontWeight: "bold" }}>{item.title}</Text>
                    </TouchableOpacity>
                )
            })
            }

        </View>
    )

};

const styles = StyleSheet.create({

});

export default BossFooter;
