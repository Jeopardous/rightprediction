import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View, TouchableOpacity,
    Text, Button,
    StatusBar, Dimensions
} from 'react-native';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome5'
import Fonts from '../../Constants/FontSize'
import BossColors from '../../Constants/BossColors'
import BossList from './BossList';
import { FlatList } from 'react-native-gesture-handler';


const { width, height } = Dimensions.get("window")

const BossBody = (props) => {
    const {
        navigation,
        bodyStyle,

    } = props

    const [listData, setListData] = useState([
        {
            name: "Adarsh",
            surname: "Sharma"
        },
        {
            name: "Alok",
            surname: "Nath Dash"
        },
        {
            name: "Kv",
            surname: "Prasad"
        }, {}, {}

    ])
    return (
        <View style={[{
            width: "100%", height: "82%", flexDirection: "row", backgroundColor: BossColors.AppBackground,
            paddingHorizontal: 5, paddingVertical: 5
        },
            bodyStyle]}>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={listData}
                numColumns={3}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (
                    <BossList
                        listType={
                            {
                                type: "grid",
                                numColumns: 3,
                                style: { height: 60 },
                                imageStyle: { borderRadius: 360 },
                                textContainerStyle: {},
                                titleStyle: {},
                                descriptionStyle: {},
                                iconName: "angle-right",
                                extraFields: [],
                                iconSize: 24,
                                iconStyle: { color: BossColors.IconColor }
                            }
                        }
                        item={item}
                        index={index}
                        onPress={() => { navigation.openDrawer() }}
                        onIconPress={() => { navigation.openDrawer() }}
                        navigation={navigation}
                        promotionIconName="bullseye"
                        promotionView={true}
                    />
                )}
            />
            {/* <BossList
                data={listData}


                gridStyle={{}}

            />
 */}
        </View>
    )

};

const styles = StyleSheet.create({

});

export default BossBody;
