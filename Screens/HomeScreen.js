import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text, Button,
    StatusBar, FlatList
} from 'react-native';
import BossHeader from '../Components/ScreenParts/BossHeader.js';
import BossColors from '../Constants/BossColors'
import BossFooter from '../Components/ScreenParts/BossFooter.js';
import Fonts from '../Constants/FontSize'
import BossBody from '../Components/ScreenParts/BossBody.js';
import BossList from '../Components/ScreenParts/BossList.js';


const HomeScreen = ({ navigation }) => {

    const [listData, setListData] = useState([
        {
            name: "Adarsh",
            surname: "Sharma"
        },
        {
            name: "Alok",
            surname: "Nath Dash"
        },
        {
            name: "Kv",
            surname: "Prasad"
        },
        {
            name: "Shekhar",
            surname: "Sir"
        },
        {
            name: "Nusrat",
            surname: "Bandriyaaa"
        }

    ])
    return (
        <View style={{ flex: 1 }}>
            <BossHeader
                navigation={navigation}
                headerLeftIconName={"bars"}
                headerLeftIconStyle={{}}
                /* headerRightIcons={[
                    {
                        name: "info",
                        onPress: () => { navigation.openDrawer() }
                    },
                    {
                        name: "search",
                        onPress: () => { navigation.openDrawer() }
                    },

                ]} */
                headerRightIconsStyle={{}}
                headerTitle={"BossBaby"}
                headerTitleStyle={{}}
                headerStyle={{ backgroundColor: BossColors.HeaderColor }}
                onPress={() => { navigation.openDrawer() }}
            />
            <View style={[{
                width: "100%", height: "100%", flexDirection: "row", backgroundColor: BossColors.AppBackground,
                paddingHorizontal: 5, paddingVertical: 5
            }]}>

                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={listData}
                    numColumns={2} /* Only for grid listType*/
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (
                        <BossList
                            listType={
                                {
                                    type: "grid",
                                    numColumns: 2,
                                    style: { height: 80 },
                                    imageStyle: { borderRadius: 360 },
                                    textContainerStyle: {},
                                    extraFields: [],
                                    titleStyle: {},
                                    descriptionStyle: {},
                                    iconName: "angle-right", iconSize: 24,
                                    iconStyle: { color: BossColors.IconColor }
                                }
                            }
                            item={item}
                            index={index}
                            onPress={() => { navigation.openDrawer() }}
                            onIconPress={() => { navigation.openDrawer() }}
                            navigation={navigation}
                            promotionIconName="bullseye"
                            promotionView={true}
                        />
                    )}
                />
            </View>
            {/*             <BossFooter
                navigation={navigation}
                footerButtons={[
                    {
                        title: "Save",
                        titleSize: Fonts.LARGE_FONT,
                        iconName: "share",
                        iconSize: 20,
                        iconDirection: "left",
                        onPress: () => { navigation.openDrawer() }
                    },
                    {
                        title: "Next",
                        titleSize: Fonts.LARGE_FONT,
                        iconName: "angle-right",
                        iconSize: 20,
                        iconDirection: "right",
                        onPress: () => { navigation.openDrawer() }
                    }
                ]}
                headerStyle={{ backgroundColor: BossColors.HeaderColor }}
                onPress={() => { navigation.openDrawer() }}
            /> */}
        </View >
    )

};

const styles = StyleSheet.create({

});

export default HomeScreen;
