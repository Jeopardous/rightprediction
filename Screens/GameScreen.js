import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text, Button,
    StatusBar,
} from 'react-native';

const GameScreen = ({ route, navigation }) => {
    /* const { otherParams } = route.params;
    console.log("otherparams::", otherParams) */
    return (
        <View>
            <Text>GameScreen</Text>
            <Button title={"GO to Home"}
                onPress={() => {
                    navigation.navigate("Home")
                }}>

            </Button>
            {/* {otherParams && <Text>Player Data:{JSON.stringify(otherParams)}</Text>} */}
        </View>
    )

};

const styles = StyleSheet.create({

});

export default GameScreen;
