import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text, Button,
  StatusBar,
} from 'react-native';
import NetInfo, { useNetInfo } from "@react-native-community/netinfo";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import AwesomeIcon from 'react-native-vector-icons/FontAwesome5'
import ErrorLoader from './Components/ErrorLoader';
import HomeScreen from './Screens/HomeScreen';
import GameScreen from './Screens/GameScreen';
import BottomTab from './Components/BottomTab';
NetInfo.configure();



/* Drawer and bottom tab combine Navigation start */
const Drawer = createDrawerNavigator();
const App = () => {
  const netinfo = useNetInfo();
  const [isLoading, setLoading] = useState(false)

  if (isLoading) {
    return (<ErrorLoader retry={!netinfo.isConnected} loader={true} />)
  }
  return (
    <>
      <StatusBar barStyle="light-content" />
      <NavigationContainer>
        <Drawer.Navigator initialRouteName={"Home"} >
          <Drawer.Screen name="Home" component={/* BottomTab */HomeScreen} />
          <Drawer.Screen name="Game" component={/* BottomTab */GameScreen} />
          <Drawer.Screen name="Settings" component={/* BottomTab */HomeScreen} />
          <Drawer.Screen name="Register/Login" component={/* BottomTab */GameScreen} />
        </Drawer.Navigator>
      </NavigationContainer>

    </>
  );
};

const styles = StyleSheet.create({

});

export default App;
/* Drawer and bottom tab combine Navigation End */




/* Bottom Tab Navigation Start */

/* const BotttomTab = createBottomTabNavigator();

const App = () => {
  const netinfo = useNetInfo();
  const [isLoading, setLoading] = useState(false)


  if (isLoading) {
    return (<ErrorLoader retry={!netinfo.isConnected} loader={true} />)
  }
  return (
    <>
      <StatusBar barStyle="light-content" />
      <NavigationContainer>
        <BotttomTab.Navigator

          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              if (route.name === 'Home') {
                iconName = focused
                  ? 'home'
                  : 'home';
              } else if (route.name === 'Game') {
                iconName = focused ? 'horse-head' : 'horse-head';
              }

              // You can return any component that you like here!
              return <AwesomeIcon name={iconName} size={size} color={color} />;
            },
          })}
          tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
          }}>
          <BotttomTab.Screen name="Home" component={HomeScreen} />
          <BotttomTab.Screen name="Game" component={GameScreen} />

        </BotttomTab.Navigator>
      </NavigationContainer>

    </>
  );
};

const styles = StyleSheet.create({

});

export default App;
 */
/* Bottom Tab Navigation End */






/* Stack Navigation Start */

{/* const Stack = createStackNavigator();


/* const LogoTitle=()=>{
  return (
    <Image
      style={{ width: 50, height: 50 }}
      source={require('@expo/snack-static/react-native-logo.png')}
    />
  );
} 
const App = () => {
  const netinfo = useNetInfo();
  const [isLoading, setLoading] = useState(false)


  if (isLoading) {
    return (<ErrorLoader retry={!netinfo.isConnected} loader={true} />)
  }
  return (
    <>
      <StatusBar barStyle="light-content" />
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home" screenOptions={{
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}>
          <Stack.Screen name="Home" component={HomeScreen} options={{
            title: 'Home', headerRight: () => (
              <TouchableHighlight
                onPress={() => alert('This is a button!')}
                style={{ width: 30, height: 30, backgroundColor: "#dddddd", justifyContent: "center", alignItems: "center", borderRadius: 3, marginRight: 10 }}
              >
                <Text>Info</Text>
              </TouchableHighlight>
            ),
          }}
          // options={{ headerTitle: props => <LogoTitle {...props} /> }}  //--------------->user for custom header
          />
          <Stack.Screen name="Game" component={GameScreen} />
        </Stack.Navigator>
      </NavigationContainer>

    </>
  );
};

const styles = StyleSheet.create({

});

export default App; */}

/* Stack Navigation End */